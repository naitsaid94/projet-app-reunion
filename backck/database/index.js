const mongoose = require('mongoose');

mongoose
  .connect(
    'mongodb://localhost:27017/projet-db',
    {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }
  )
  .then(() => {
    console.log('connexion ok !');
  })
  .catch((err) => {
    console.log(err);
  });
