const mongoose = require('mongoose');

const schema = mongoose.Schema;

var simplecrypt = require('simplecrypt');
var sc = simplecrypt();

const userSchema = schema({
  local: {
    email: { type: String, required: true, unique: true },
    password: { type: String },
  },
  username: String,
  name:String,
  prenom: String,
  poste: String,
  date: Date 
});


userSchema.statics.hashPassword = async (password) => {
  try {
     return sc.encrypt(password);
  } catch(e) {
    throw e
  }
}

userSchema.methods.comparePassword = function(password) {
   return (sc.encrypt(password) === this.local.password);
}

const User = mongoose.model('user', userSchema);

module.exports = User;
