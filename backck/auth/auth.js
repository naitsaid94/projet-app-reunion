const passport = require('passport');
const User = require('../database/models/user.model');
const localStrategy = require('passport-local').Strategy;
const { findUserPerEmail } = require('../queries/user.queries');
const { createUser } = require('../queries/user.queries')


const JWTstrategy = require('passport-jwt').Strategy;
const ExtractJWT = require('passport-jwt').ExtractJwt;


passport.use(
    'signup',
    new localStrategy(
        {
            usernameField: 'email',
            passwordField: 'password'
        },
        async (email, password, done) => {
            try {
                const user = await createUser({ email, password });
                // A quoi sert ce return
                return done(null, user);
            } catch (error) {
                done(error);
            }
        }
    )
);

// ...

passport.use(
    'login',
    new localStrategy(
        {
            usernameField: 'email',
            passwordField: 'password'
        },
        async (email, password, done) => {
            try {
                const user = await findUserPerEmail(email);

                if (!user) {
                    return done(null, false, { message: 'User not found' });
                }

                const validate = await user.comparePassword(password);

                if (!validate) {
                    return done(null, false, { message: 'Wrong Password' });
                }

                return done(null, user, { message: 'Logged in Successfully' });
            } catch (error) {
                return done(null, false, {message:"find error"});
            }
        }
    )
);

passport.use(
    new JWTstrategy(
        {
            secretOrKey: 'TOP_SECRET',
            jwtFromRequest: ExtractJWT.fromUrlQueryParameter('secret_token')
        },
        async (token, done) => {
            try {
                return done(null, token.user);
            } catch (error) {
                done(error);
            }
        }
    )
);

