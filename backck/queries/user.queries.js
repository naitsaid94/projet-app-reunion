const User = require('../database/models/user.model');

exports.createUser = async (body) => {
  try {
    const hashedPassword = await User.hashPassword(body.password);
    const user = new User({ 
      username: body.username,
      name: body.name,
      prenom: body.prenom,
      poste: body.poste,
      date: body.date,
      local: {
        email: body.email,
        password: hashedPassword
      }
    })
    return user.save();
  } catch(e) {
    throw e;
  }
}

exports.findUserPerEmail = (email) => {
  return User.findOne({ 'local.email': email }).exec();
}

exports.findUserPerId = (id) => {
  return User.findOne({ _id: id }).exec();
}

exports.controller={
    getAll: async (req, res, next) => {
        let users = await User.find({});
        res.json(users);},

    addOne: async (req, res, next) => {
        let user = await User.create(req.body)
        res.json(user);},

    deleteAll: async (req, res, next) => {
        let resp = await User.remove({});
        res.json(resp);},
    getOne: async (req, res, next) => {
        let user = await User.findById(req.params.id);
        res.json(user);},
    updateOne: async (req, res, next) => {
        let user = await User.findByIdAndUpdate(req.params.id, req.body,{ new:true });
        res.json(user);},
    deleteOne: async (req, res, next) => {
        let resp = await User.findByIdAndRemove(req.params.id);
        res.json(resp);},
   };
