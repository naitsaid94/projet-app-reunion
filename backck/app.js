const path = require('path');
const express = require('express'); 
const cors = require('cors');
require('./database');
var logger = require('morgan');
const router = require('./routes');
const cookieParser = require('cookie-parser');
const app = express();
const { ensureAuthenticated } = require('./config/security.config');
exports.app = app;
app.use(cors());
app.use(cookieParser());

require('./config/jwt.config');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(logger('dev'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(router);

//app.listen(3000);
module.exports = app;