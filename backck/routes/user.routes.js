const express = require("express");
const { userNew, userCreate } = require('../controllers/user.controller');
const {controller,getAll, createUser} = require("../queries/user.queries");
const router = express.Router();

/*router.get('/new', userNew);
router.post('/', userCreate);
*/

router.all("/",(req, res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
})
router.get("/new", (req, res, next) => {console.log("coucou");next()}, controller.getAll);
router.post("/", userCreate );
router.delete("/",controller.deleteAll);

router.get("/:id",controller.getOne);
router.put("/:id",controller.updateOne);
router.delete("/:id",controller.deleteOne);


module.exports = router;