const Reservation = require("../models/reservation.model");
//création du controller des réservations
let controller={
    // récupération de tous les réservations existants dans la base de données
    getAll: async (req, res, next) => {
        let reservations = await Reservation.find({});res.json(reservations);},

    // ajout d' une réservation
    addOne: async (req, res, next) => {
        let reservation = await Reservation.create(req.body)
        res.json(reservation);},

    // suppression de tous les  réservations
    deleteAll: async (req, res, next) => {
        let resp = await Reservation.remove({});
        res.json(resp);},

    // récupération d'une réservation avec son identifiant en paramètre
    getOne: async (req, res, next) => {
        let reservation = await Reservation.findById(req.params.id);
        res.json(reservation);},

    // mise à jour d'une réservation en précisant en paramètre son identifiant
    updateOne: async (req, res, next) => {
        let reservation = await Reservation.findByIdAndUpdate(req.params.id, req.body,{ new:true });
        res.json(reservation);},
        
    // suppression d'une réservation avec son identifiant en paramètre
    deleteOne: async (req, res, next) => {
        let resp = await Reservation.findByIdAndRemove(req.params.id);
        res.json(resp);},
   };
   
   module.exports = controller;
   