
const User = require("../models/user.model");

let controller={
    getAll: async (req, res, next) => {
        let users = await User.find({});
        res.json(users);},

    addOne: async (req, res, next) => {
        let user = await User.create(req.body)
        res.json(user);},

    deleteAll: async (req, res, next) => {
        let resp = await User.remove({});
        res.json(resp);},
    getOne: async (req, res, next) => {
        let user = await User.findById(req.params.id);
        res.json(user);},
    updateOne: async (req, res, next) => {
        let user = await User.findByIdAndUpdate(req.params.id, req.body,{ new:true });
        res.json(user);},
    deleteOne: async (req, res, next) => {
        let resp = await User.findByIdAndRemove(req.params.id);
        res.json(resp);},
};

module.exports = controller;

