const express = require ("express");
const router = express.Router();
const mongoose = require("mongoose");
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const JWT_SIGN_SECRET ='secret';

// création de la route de création de compte
router.post('/signup', (req, res, next) => {
    //hachage du mot de passe avec le bcrypt
    bcrypt.hash(req.body.password, 10, (err,hash) => {
            if (err) {
                return res.status(500).json({
                    error:err
                });
            } else{
                const user = new User({
                    _id: new mongoose.Types.ObjectId(),
                    email: req.body.email,
                    password : hash
                });
                user
                .save()
                .then(result => {
                    console.log(result);
                    res.status(201).json({
                        message:'User créé'
                    });
                })
                .catch( err => {
                    console.log(err);
                    res.status(500).json({
                        error: err
                    });
                });
            }
        

    });
});
// création de la route de l'authentification
router.post("/login",(req, res, next) => {
    User.find({email : req.body.email})
     .exec()
     .then(user =>{
         if (user.length <1) {
           return res.status(404).json({
               message:'authentification échouée'
           });
         }
         bcrypt.compare(req.body.password, user[0].password, (err,result) => {
            if (err) {
                return res.status(401).json({
                    message:'authentification échouée'
                });
            }
            // génération du token si l'authentification est réussie
            if(result){
                const token = jwt.sign(
                    {
                    email: user [0].email,
                    userId: user[0]._id
                   },
                JWT_SIGN_SECRET,
                  {
                    expiresIn: "1h"
                  })
               return res.status(200).json ({
                   message:'authentification réussie',
                   token:token
               });
            }
         });
     })
     .catch(err => {
         console.log(err);
         res.status(500).json({
             error: err
         });
     });
});

module.exports = router;