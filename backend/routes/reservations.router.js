const express = require("express");
const controller = require("../controllers/reservations.controller");
var router = express.Router();
// création des routes des réservations
router
.route("/")
.all((req,res, next) => {
    res.statusCode = 200;
    res.setHeader("Content-Type", "text/plain");
    next();
})
.get(controller.getAll)

.post(controller.addOne)

.delete(controller.deleteAll);
router.route("/:id").get(controller.getOne).post(controller.addOne).put(controller.updateOne).delete(controller.deleteOne) ;


module.exports = router;
