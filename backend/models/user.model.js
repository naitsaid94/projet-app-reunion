const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const validator = require('validator');

// création du schéma de l' authentification
const userSchema = new Schema(
    {
        // vérification du mail avec le validator
        email: { type: String, required: true, unique: true, validate: value => {
            if(!validator.isEmail(value)){
              throw new Error({error: "Adresse mail non valide"})
            }
          }
        },
        password: { type: String },
        isAdmin:0
    }
);



const User = mongoose.model('user', userSchema);

module.exports = User;
