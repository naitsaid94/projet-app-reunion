const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const reservationSchema = new Schema(
{
fullName: { type: String },
NumSalle: { type: Number },
date: { type: Date },
heure: {type: String}
},
{ collection: "reservations", timestamps: true}
);

module.exports= mongoose.model("Reservation", reservationSchema);
