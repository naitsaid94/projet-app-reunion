import User_account from "./components/User_account";
import ListeSalle from './components/listesalle/Liste';
import ListeReservation from './components/listereservation/Liste';
import SignIn from './components/signin/SignIn';
import SignUp from './components/signup/SignUp';
import Ajoutreservation from './components/ajoutReservation/ajoutReservation';
import AjoutSalle from'./components/ajoutsalle/AjoutSalle';
import PageAccueil from "./components/page_accueil/page_accueil";

import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';


function App() {
    return (
        <div>
            {/*<h1>This is our app</h1>*/}
            <BrowserRouter>
                {/*<nav>
                    <ul>
                    <li><Link to="/">Signin</Link></li>
                    <li><Link to="/signup">Signup</Link></li>
                    <li><Link to="/home">Home</Link></li>
                    </ul>
                    </nav>*/}
                    <Switch>
                    <Route path="/userAccount">
                    <User_account/>
                    </Route>
                    <Route path="/ajoutSalle">
                    <AjoutSalle/>
                    </Route>
                    <Route path="/listeSalle">
                    <ListeSalle/>
                    </Route>
                    <Route path="/ajoutReservation">
                    <Ajoutreservation />
                    </Route>
                    <Route path="/listeReservation">
                    <ListeReservation />
                    </Route>
                    <Route path="/home">
                    <PageAccueil />
                    </Route>
                    <Route path="/signup">
                    <SignUp />
                    </Route>
                    <Route path="/">
                    <SignIn />
                    </Route>
                    </Switch>
                    </BrowserRouter>
                    </div>
                    )
                }

export default App
