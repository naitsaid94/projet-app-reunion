import React from 'react';
import ReactDom from 'react-dom';
import { Formik, Field, ErrorMessage } from 'formik';
import axios from 'axios';
import * as Yup from 'yup';
import logo from './sign2.png';
import  Style from './style.css';

import { BrowserRouter, Route, Switch, Link } from 'react-router-dom';


const today = new Date();
const ComposantErreur = props => (
    <div className="text-danger">{props.children}</div>
  );
  
  const ComposantInput = ({
    field, 
    form: { touched, errors },
    ...props
  }) => (
    <div className="form-group">
      <label> { props.label } </label>
      <input type="text" {...props} className="form-control" {...field} />
    </div>
  );
   
  
  
export default class SignIn extends React.Component{
    submit = (values, actions) => {
        console.log(values);
        setTimeout(() => {
          actions.isSubmitting = false;
          actions.resetForm();
        }, 1000);
      }
      userSchema = Yup.object().shape({
        email: Yup.string().email("L'email doit être valide").required('champ Requis'),
        password: Yup.string().min(5, 'Trop court').required('champ Requis')
      }); 
render(){
return(
<div className="container-fluid pt-5 
      d-flex flex-column justify-content-center align-items-center mt-auto ">
        <Formik onSubmit={(values, actions) => {
    axios.post('https://localhost:5000/auth/signin',values)
       .then(response => {
         actions.setSubmitting(false);
         actions.resetForm();
         console.log(values);
       })
       .catch(error => {
         actions.isSubmitting= false;
       });
     }}
        
          initialValues={{email: '', password: ''}}
          validationSchema={this.userSchema}
        >
          {({values,
          handleBlur,
          handleChange,
          handleSubmit,
          isSubmitting,
          errors,
          touched}) => (
              
            
              <div className="d-flex flex-column shadow p-2 mb-3 bg-white rounded " >
                  
            <form onSubmit={ handleSubmit } className="bg-white border p-4 d-flex flex-column">
            <div className="h3 d-flex justify-content-center"> Authentification </div>
            <div className="d-flex justify-content-center">
            <img alt="signup" className="mx-auto d-block w-50" src={logo} />  
            </div>
            <Field name="email" label="Email" type="email" onChange={handleChange} placeholder="Votre Adreese Email" autoFocus component={ComposantInput} />
            <ErrorMessage name="email" component={ComposantErreur} />
        
            <Field name="password"  label="Mot de passe" onChange={handleChange} placeholder="Mot de Passe" component={ComposantInput} />
            <ErrorMessage name="password" component={ComposantErreur} />
            <div className="p-1 d-block">
                <a className="d-block" href="#">Mot de passe oublié?</a>   
                {/*<Link to="/signup">S'incrire</Link>*/}
            </div>
              <div className="p-2 d-flex justify-content-center">
                  <button type="submit" className=" btn btn-primary" disabled={isSubmitting}>
                S'authentifier
              </button>
                  {/*<Link to="/home" type="submit" className="btn btn-primary" disabled={isSubmitting}>
                      S'authentifier
                  </Link>*/}
              </div>
            </form>
            </div>
          )}
        </Formik>
             

      </div>
      

)

}


}
