import React from 'react';

export default class DescUser extends React.Component {

    render(){
        return(
        <div className="w-25 bg-light p-4 d-flex flex-column position-fixed top-1 end-0 shadow-sm ">
           <h5>{ this.props.user.name} {this.props.user.prenom } </h5>
           <hr className="w-100"/>
            <div >
             <img alt="movie" className="mx-auto d-block w-100 " src={this.props.user.poste } />
            </div>
            <hr className="w-100"/>
            <h6> INFORMATIONS</h6>
            <hr className="w-50"/>
            <span className="text-secondary p-1">AGE: {this.props.user.prenom } </span>
            <span className="text-secondary p-1">Email: {this.props.user.local.email}</span>
            <span className="text-secondary p-1">Poste: {this.props.user.poste}</span>
          
             </div>
        );
    }
}
