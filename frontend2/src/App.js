import User_account from "./components/User_account";
import Liste from './components/listeutilisateur/Liste';
import ListeR from './components/listereservation/Liste';
import ListeS from './components/listesalle/Liste';
import AjoutU from './components/signup/SignUp';
import axios from 'axios';
import AjoutS from './components/ajoutsalle/AjoutSalle';
import Signin from './components/signin/SignIn';

import ListeAR from './components/ajoutReservation/ajoutReservation';


function App() {
  return (
    <div>
      <Signin />
      <AjoutU />
      <Liste/>
      </div>
      )
}

export default App
